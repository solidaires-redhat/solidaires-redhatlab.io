---
title: "Nous contacter"
date: "{{ .Date }}"
draft: false
---

# Délégué Syndical

### Haïkel Guémar `hguemar` ou `number80`

# Permanence en visio-conférence

À Venir

# Par mail

### `redhat (at) solidairesinformatique (dot) org`

# Site internet

### [solidaires-redhat.org](https://www.solidaires-redhat.org)

# Réseaux sociaux

### [@SolidairesRedHat@syndicat.solidaires.org](https://syndicat.solidaires.org/@SolidairesRedHat) (Mastodon)
### [@unionsolidaires@syndicat.solidaires.org](https://syndicat.solidaires.org/@unionsolidaires) (Mastodon)
### [@RedHatSolinfo](https://twitter.com/RedHatSolinfo) (Twitter/X)

# Pour Adhérer ?

### [⬇️ Bulletin d'adhésion Solidaires Informatique ⬇️](https://solidairesinformatique.org/wp-content/uploads/2022/06/BulletinAdhesion202206.pdf)
