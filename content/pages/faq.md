---
title: "Foire au questions"
date: "{{ .Date }}"
draft: false
---

Nous sommes des salarié⋅es de l'UES Red Hat (Red Hat France et Enovance), souhaitant nous regrouper pour mieux être représenté.

Nous sommes une section du syndicat **[Solidaires Informatique](https://solidairesinformatique.org/)**, membre de la [Union syndicale Solidaires](https://solidaires.org/).

---

1. [Pourquoi se syndiquer ?](#pourquoi_se_syndiquer)
1. [Pourquoi Solidaires Informatique ?](#pourquoi_solidaires_informatique)
1. [Nos valeurs ?](#nos_valeurs)

---

1. # Pourquoi se syndiquer ? {#pourquoi_se_syndiquer}
    * Se syndiquer c’est refuser la fatalité, c’est faire partie de ces salariés qui veulent changer le cours des choses.
1. # Pourquoi Solidaires Informatique ? {#pourquoi_solidaires_informatique}
    * Rejoindre Solidaires Informatique, c’est la possibilité d’apprendre à s’organiser, c’est la possibilité de discuter, de réfléchir collectivement, de rassembler les énergies. Rappelons que Solidaires est indépendant de tout parti politique.
    Solidaires Informatique développe son action en fonction des seuls intérêts des salariés.
Pour nous, faire du syndicalisme est un choix non un plan de carrière, ni une profession.
A ce titre, nous refusons de bénéficier d’avantages particuliers liés à notre activité syndicale.
1. # Nos Valeurs ? {#nos_valeurs}
    * Nous sommes un syndicat indépendant qui défends nos acquis sociaux, mais aussi les droits des chômeur·euse·s, des sans droits, pour l’égalité des hommes et des femmes, contre le racisme et l’extrême-droite 
