---
title: "Bienvenue Solidaires Redhat"
author: "Haïkel"
type: ""
date: 2023-04-29T18:28:15+02:00
subtitle: ""
image: ""
tags: [bienvenue]
---

Solidaires Red Hat est la 1ère section syndicale créé à Red Hat France, officialisée en 2018.
Nous sommes affilié à [Solidaires Informatique](https://solidairesinformatique.org/)

Ce site est encore en cours de construction :)
